/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   map_lock.h
 * Author: eatakishiyev
 *
 * Created on November 9, 2017, 6:12 PM
 */

#ifndef MAP_LOCK_H
#define MAP_LOCK_H
#include <atomic>
#include <mutex>
#include <condition_variable>

using namespace std;

class map_lock {
private:


    mutex __m;
    condition_variable_any con_var;

public:
    atomic_bool __locked{false};
    long unlock_task = -1;
    atomic_long lock_id;

    map_lock() {

    }

    ~map_lock() {

    }

    void lock() {
        __m.lock();
        __locked.exchange(true);
    }

    bool lock(int lock_id) {
        if (lock_id > 0 && __locked.load()) {
            if (lock_id != this->lock_id.load()) {
                return -1;
            }
        }

        __m.lock();
        __locked.exchange(true);
        return true;
    }

    void unlock() {
        __locked.exchange(false);
        this->lock_id.exchange(-1);
        __m.unlock();

    }

    bool unlock(long lock_id) {
        if (lock_id == 0 && !__locked.load()) {
            return true;
        }

        if (this->lock_id.load() == lock_id) {
            this->__locked.exchange(false);
            this->lock_id.exchange(-1);
            __m.unlock();
            return true;
        }

        return false;
    }

    bool tryLock() {
        __locked.exchange(__m.try_lock());
        return __locked.load();
    }
};

#endif /* MAP_LOCK_H */


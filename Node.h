/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.h
 * Author: root
 *
 * Created on August 5, 2016, 9:58 PM
 */

#ifndef NODE_H
#define NODE_H
#include "value_c.h"
#include <mutex>
#include <chrono>

using namespace std;
using namespace std::chrono;

class Node {
public:
//    volatile atomic_int lock_id;
    atomic_int lock_count;
    atomic_long lock_release_time;
    std::condition_variable convar;
    long mutex_lock_task = -1;

    Node() {

    }

    ~Node() {
        key = 0;
        lock_count = 0;
        next = 0;
        current_lock_by = 0;
        delete value;
        value = nullptr;
    }

    Node(long key, value_c* value) {
        this->key = key;
        this->value = value;
        this->lock_count = 0;
        this->next = NULL;
        this->current_lock_by = 0;
    }

    long getKey()const {
        return this->key;
    }

    void seKey(long key) {
        this->key = key;
    }

    value_c* getValue() const {
        return this->value;
    }

    void setValue(value_c* value) {
        this->value = value;
    }

    Node* getNext() const {
        return this->next;
    }

    void setNext(Node* next) {
        this->next = next;
    }

//    int lock(unique_lock<mutex> &ul, int timeout, int lock_id) {
//
//        if (lock_id > 0 && lock_count > 0) {
//            if (lock_id != this-> lock_id) {
//                return -1;
//            }
//        }
//
//        if (lock_count > 0) {
//            auto delta = lock_release_time -
//                    duration_cast<chrono::seconds>(system_clock::now().time_since_epoch()).count();
//            if (delta) {
//                convar.wait_for(ul, seconds(delta));
//                lock_count--;
//            }
//        }
//
//        this->lock_release_time = duration_cast<seconds>(system_clock::now().time_since_epoch()).count()
//                + timeout;
//        lock_count++;
//        this->lock_id = lock_id;
//
//        return lock_id;
//    }

//    void lock(unique_lock<mutex> &ul, int timeout) {
//        //lock_guard<mutex> u(_mutex);
//        if (lock_count.load() > 0) {
//            auto delta = lock_release_time -
//                    duration_cast<chrono::seconds>(system_clock::now().time_since_epoch()).count();
//            //if (delta) {
//            cout << "lock: delta = " << delta << "\n" << endl;
//            convar.wait_for(ul, seconds(delta));
//            lock_count.operator--(1);
//            //}
//        }
//        lock_count.operator++(1);
//        this->lock_release_time = duration_cast<seconds>(system_clock::now().time_since_epoch()).count()
//                + timeout;
//        cout << "lock: lock_release_time = " << this->lock_release_time << endl;
//
//    }

//    bool unlock(int lock_id) {
//        if (this->lock_count.load() <= 0 && lock_id == 0)
//            return true;
//        if (this->lock_id.load() == lock_id) {
//            //            lock_count--;
//            //            this->current_lock_by = 0;
//            //            convar.notify_one();
//            return true;
//        }
//        return false;
//    }
//
//    bool tryLock(unique_lock<mutex> &ul, long timeout) {
//        if (lock_count > 0) {
//            return false;
//        }
//        this->lock(ul, timeout);
//        return true;
//    }

    //    void check_for_lock() {
    //        //        if (thread_id_hasher(std::this_thread::get_id()) == current_lock_by) {
    //        //            return;
    //        //        }
    //
    //
    //        while (lock_count > 0) {
    //            long delta = time(NULL) - this->last_lock_time;
    //            if (delta >= lock_time_out) {
    //                lock_count--;
    //            }
    //            std::this_thread::sleep_for(std::chrono::microseconds(1));
    //        }
    //    }

private:
    long key;
    value_c * value;
    Node * next;
    unsigned long current_lock_by;
    atomic_long lock_time_out;

    std::thread owned_by;
    std::hash<std::thread::id> thread_id_hasher;
    std::mutex _mutex;

};
#endif /* NODE_H */


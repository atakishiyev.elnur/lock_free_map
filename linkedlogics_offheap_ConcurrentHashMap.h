/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class linkedlogics_offheap_ConcurrentHashMap */

#ifndef _Included_linkedlogics_offheap_ConcurrentHashMap
#define _Included_linkedlogics_offheap_ConcurrentHashMap
#ifdef __cplusplus
extern "C" {
#endif
    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    setEvictionHandler
     * Signature: (ILlinkedlogics/offheap/ConcurrentHashMap/EvictionNotificationHandler;)V
     */
    JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_setEvictionHandler
    (JNIEnv *, jobject, jint, jobject);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    put
     * Signature: (IJ[BI)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_put__IJ_3BI
    (JNIEnv *, jobject, jint, jlong, jbyteArray, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    put
     * Signature: (IJ[BJI)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_put__IJ_3BJI
    (JNIEnv *, jobject, jint, jlong, jbyteArray, jlong, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    get
     * Signature: (IJ)[B
     */
    JNIEXPORT jbyteArray JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_get
    (JNIEnv *, jobject, jint, jlong);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    tryLock
     * Signature: (IJJ)I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_tryLock
    (JNIEnv *, jobject, jint, jlong, jlong);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    lock
     * Signature: (IJJ)I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_lock__IJJ
    (JNIEnv *, jobject, jint, jlong, jlong);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    lock
     * Signature: (IJJI)I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_lock__IJJI
    (JNIEnv *, jobject, jint, jlong, jlong, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    unlock
     * Signature: (IJI)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_unlock
    (JNIEnv *, jobject, jint, jlong, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    remove
     * Signature: (IJ)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_remove__IJ
    (JNIEnv *, jobject, jint, jlong);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    remove
     * Signature: (IJI)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_remove__IJI
    (JNIEnv *, jobject, jint, jlong, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    rescheduleEviction
     * Signature: (IJJ)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_rescheduleEviction
    (JNIEnv *, jobject, jint, jlong, jlong);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    createMap
     * Signature: ()I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_createMap__
    (JNIEnv *, jobject);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    createMap
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_createMap__I
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    begin
     * Signature: (I)V
     */
    JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_begin
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    suspendEviction
     * Signature: (I)V
     */
    JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_suspendEviction
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    resumeEviction
     * Signature: (I)V
     */
    JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_resumeEviction
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    next
     * Signature: (I)J
     */
    JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_next
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    hasMore
     * Signature: (I)Z
     */
    JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_hasMore
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    printStat
     * Signature: (I)V
     */
    JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_printStat
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    evictedCount
     * Signature: (I)J
     */
    JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_evictedCount
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    size
     * Signature: (I)I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_size
    (JNIEnv *, jobject, jint);


    JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_sizeInBytes
    (JNIEnv *, jobject, jint);
    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    consumeEviction
     * Signature: (I)J
     */
    JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_consumeEviction
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    consumeEvictionNonBlocking
     * Signature: (I)J
     */
    JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_consumeEvictionNonBlocking
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    destroy
     * Signature: (I)V
     */
    JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_destroy
    (JNIEnv *, jobject, jint);

    /*
     * Class:     linkedlogics_offheap_ConcurrentHashMap
     * Method:    checkLock
     * Signature: (IJI)I
     */
    JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_checkLock
    (JNIEnv *, jobject, jint, jlong, jint);

#ifdef __cplusplus
}
#endif
#endif

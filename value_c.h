/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   value_c.h
 * Author: root
 *
 * Created on August 5, 2016, 9:57 PM
 */

#ifndef VALUE_C_H
#define VALUE_C_H

#include <stddef.h>
#include "map_eviction_data.h"
#include <mutex>
#include "Scheduler.h"

class value_c {
public:
    
    value_c(long key, int length, char* value, long version, map_eviction_data * eviction_data) {
        this->key = key;
        this->length = length;
        this->val = value;
        this->version = version;
        this->eviction_data = eviction_data;
        
    }

    ~value_c() {
        if (val) {
            free(val);
            val = nullptr;
        }
        version = 0;
        length = 0;
    }

    int getLength() {
        return this->length;
    }

    void setLength(int length) {
        this->length = length;
    }

    char* getValue() {
        return this->val;
    }

    void setValue(char* value) {
        this->val = value;
    }

    long getVersion() {
        return this->version;
    }

    void setVersion(long version) {
        this->version = version;
    }

    long getKey() {
        return this->key;
    }

    void setKey(long key) {
        this->key = key;
    }

    long task = -1;

private:

    long key = 0;
    int length = 0;
    char * val = NULL;
    long version = 0;
    map_eviction_data* eviction_data;

};
#endif /* VALUE_C_H */


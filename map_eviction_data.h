/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   map_eviction_data.h
 * Author: root
 *
 * Created on July 25, 2016, 11:15 AM
 */

#ifndef MAP_EVICTION_DATA_H
#define MAP_EVICTION_DATA_H
#include <jni.h>
#include <atomic>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;
class map_eviction_data {
public:

    map_eviction_data() : eviction_queue_con_var(), eviction_queue() {
//        eviction_queue = new queue<long>();
    }

    map_eviction_data(const map_eviction_data &orig) {
        cout << "Copy constructor called" << endl;
    }

    ~map_eviction_data() {
        cout << "Destructor called" << endl;
    }

    long consumeEvictionNonBlocking() {
        unique_lock<mutex> _lock(eviction_queue_mutex);
        if (eviction_queue.empty())
            return -1;
        long key = eviction_queue.front();
        eviction_queue.pop();
        return key;
    }

    long consumeEvictionBlocking() {
        unique_lock<mutex> _lock(eviction_queue_mutex);
        eviction_queue_con_var.wait(_lock, [this] {
            return !eviction_queue.empty();
        });
        long key = eviction_queue.front();
        eviction_queue.pop();
        return key;
    }

    mutex eviction_queue_mutex;
    condition_variable eviction_queue_con_var;
    queue<long> eviction_queue;
    //    
    int eviction_count = 0;
    atomic_int total_scheduled{0};
//    jobject listener;
//    jmethodID methodId;
//    constexpr static char* eviction_handler_method = (char*) "onEvict";
//    constexpr static char* eviction_handler_method_params = (char*) "(J)V";
//    JavaVM * jvm;
};

#endif /* MAP_EVICTION_DATA_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   lock_free_map.h
 * Author: root
 *
 * Created on August 5, 2016, 9:59 PM
 */

#ifndef LOCK_FREE_MAP_H
#define LOCK_FREE_MAP_H
#include <mutex>
#include "Node.h"
#include "Scheduler.h"
#include "map_eviction_data.h"
#include <limits.h>
#include "map_lock.h"

using namespace std;

class lock_free_map {
public:

    lock_free_map() {
        this->m_table = new Node*[m_table_size];

        lock_scheduler = new Scheduler(1);
        this->locks = new map_lock*[m_table_size];

        for (int i = 0; i < m_table_size; i++) {
            this->locks[i] = new map_lock();
        }

        scheduler = new Scheduler(1);
        eviction_data = new map_eviction_data();
    }

    lock_free_map(int initial_capacity) {
        this->m_table_size = initial_capacity;
        this->m_table = new Node*[initial_capacity]();

        this->locks = new map_lock*[m_table_size];
        for (int i = 0; i < m_table_size; i++) {
            this->locks[i] = new map_lock();
        }


        scheduler = new Scheduler(1);
        lock_scheduler = new Scheduler(1);

        eviction_data = new map_eviction_data();
    }

    bool put(long key, value_c* value, long timeout) {

        int index = hasher(key) % m_table_size;


        Node * entry = m_table[index];
        Node * prev = NULL;

        while (entry != NULL && entry->getKey() != key) {
            prev = entry;
            entry = entry->getNext();
        }

        //key not exists put it
        if (entry == NULL) {
            entry = new Node(key, value);
            if (prev == NULL) {
                m_table[index] = entry;
            } else {
                prev->setNext(entry);
            }
            _size++;
            _size_in_bytes.operator+=(value->getLength());

            if (timeout > 0)
                value->task = scheduler->schedule(seconds(timeout), &lock_free_map::onExpired, this, key);

            return true;
        } else {
            //            entry->check_for_lock();
            if (entry->getValue()->getVersion() < value->getVersion()
                    || value->getVersion() < 0) {
                if (entry->getValue() != nullptr) {
                    scheduler->cancel(entry->getValue()->task);
                }

                if (value->getVersion() < 0) {
                    value->setVersion(entry->getValue()->getVersion());
                }

                _size_in_bytes.operator-=(entry->getValue()->getLength());

                delete entry->getValue();
                if (timeout > 0)
                    value->task = scheduler->schedule(seconds(timeout), &lock_free_map::onExpired, this, key);

                _size_in_bytes.operator+=(value->getLength());
                entry->setValue(value);
                return true;
            } else {
                delete value;
                return false;
            }
        }
    }

    void onExpired(long key) {
        std::unique_lock<mutex> _lock(eviction_data->eviction_queue_mutex);
        eviction_data->eviction_queue.push(key);
        eviction_data->eviction_queue_con_var.notify_one();
        eviction_data->eviction_count++;
    }

    value_c* get(long key) {
        int index = hasher(key) % m_table_size;

        Node * entry = m_table[index];

        while (entry != NULL) {
            if (entry->getKey() == key) {
                //                entry->check_for_lock();
                return entry->getValue();
            }
            entry = entry->getNext();
        }
        return nullptr;
    }

    int checkLock(long key, int lock_id) {
        int index = hasher(key) % m_table_size;

        Node * entry = __find_node(index, key);

        if (entry == NULL) {
            return -1;
        }

        if (locks[index]->lock_id.load() == lock_id) {
            return locks[index]->lock_id.load();
        } else {
            return -1;
        }
    }

    bool remove(long key) {
        int index = hasher(key) % m_table_size;

        Node * entry = m_table[index];
        Node * prev = NULL;

        while (entry) {

            if (entry->getKey() == key) {
                //                entry->check_for_lock();
                if (prev) {
                    prev->setNext(entry->getNext());
                } else {
                    if (entry->getNext() == NULL)
                        m_table[index] = NULL;
                    else
                        m_table[index] = entry->getNext();
                }

                _size--;
                _size_in_bytes.operator-=(entry->getValue()->getLength());
                if (entry->getValue() != nullptr) {
                    scheduler->cancel(entry->getValue()->task);
                }
                delete entry;
                return true;
            }

            prev = entry;
            entry = entry->getNext();
        }
        return false;
    }

    bool remove(long key, int version) {
        int index = hasher(key) % m_table_size;

        Node * entry = m_table[index];
        Node * prev = NULL;

        while (entry) {

            if (entry->getKey() == key) {

                if (entry->getValue()->getVersion() > version)
                    return false;

                //                entry->check_for_lock();
                if (prev) {
                    prev->setNext(entry->getNext());
                } else {
                    if (entry->getNext() == NULL)
                        m_table[index] = NULL;
                    else
                        m_table[index] = entry->getNext();
                }
                _size--;
                _size_in_bytes.operator-=(entry->getValue()->getLength());

                if (entry->getValue() != nullptr)
                    scheduler->cancel(entry->getValue()->task);

                delete entry;
                return true;
            }

            prev = entry;
            entry = entry->getNext();
        }
        return false;
    }

    int lock(long key, long timeout) {
        int index = hasher(key) % m_table_size;

        locks[index]->lock();
        Node* entry = __find_node(index, key);

        if (entry == NULL) {
            locks[index]->unlock();
            return -1; //no key found
        }

        int new_lock_id = lock_id_sequencer.fetch_add(1);
        if (new_lock_id >= INT_MAX)
            lock_id_sequencer.exchange(0);

        locks[index]->lock_id = new_lock_id;

        if (timeout > 0) {
            locks[index]->unlock_task = lock_scheduler->schedule(seconds(timeout), &lock_free_map::___unlock, this, key, new_lock_id);
        }

        return new_lock_id;

    }

    int lock(long key, long timeout, int lock_id) {
        int index = hasher(key) % m_table_size;

        bool result = locks[index]->lock(lock_id);

        Node* entry = __find_node(index, key);

        if (entry == NULL) {
            if (result) {
                locks[index]->unlock();
            }
            return -2;
        }

        if (result) {
            int new_lock_id = lock_id_sequencer.fetch_add(1);
            if (new_lock_id >= INT_MAX)
                lock_id_sequencer.exchange(0);

            locks[index]->lock_id.exchange(new_lock_id);

            if (timeout > 0) {
                locks[index]->unlock_task = lock_scheduler->schedule(seconds(timeout), &lock_free_map::___unlock, this, key, new_lock_id);
            }
            return new_lock_id;
        }

        return -1;
    }

    bool unlock(long key, int lock_id) {
        int index = hasher(key) % m_table_size;

        Node * entry = __find_node(index, key);

        //if (entry == NULL) {
        //    return false;
        //}

        bool unlock_result = locks[index]->unlock(lock_id);
        if (unlock_result) {
            //            cout << "Cancelled" << locks[index]->unlock_task << endl;
            lock_scheduler->cancel(locks[index]->unlock_task);
        }
        return unlock_result;
    }

    int tryLock(long key, long timeout) {
        int index = hasher(key) % m_table_size;

        bool t = locks[index]->tryLock();
        if (!t)
            return -2;

        Node* entry = __find_node(index, key);
        if (entry == NULL) {
            return -1;
        }

        int new_lock_id = lock_id_sequencer.fetch_add(1);
        if (new_lock_id >= INT_MAX)
            lock_id_sequencer.exchange(0);

        locks[index]->lock_id.exchange(new_lock_id);

        if (timeout > 0) {
            locks[index]->unlock_task = lock_scheduler->schedule(seconds(timeout), &lock_free_map::___unlock, this, key, new_lock_id);
        }
        return new_lock_id;
    }

    int size() {
        return _size;
    }

    long size_in_bytes() {
        return _size_in_bytes.load();
    }

    long consumeEvictionNonBlocking() {
        return eviction_data->consumeEvictionNonBlocking();
    }

    long consumeEvictionBlocking() {
        return eviction_data->consumeEvictionBlocking();
    }

    ~lock_free_map() {
        for (uint i = 0; i < m_table_size; i++) {
            Node* entry = m_table[i];
            while (entry) {
                Node* next = entry->getNext();
                delete entry;
                entry = nullptr;
                entry = next;
            }
            m_table[i] = nullptr;
        }

        delete[] m_table;
        delete eviction_data;
        delete scheduler;
        delete[] locks;
    }


    map_eviction_data* eviction_data;
    Scheduler *scheduler;
    Scheduler* lock_scheduler;

private:
    Node **m_table;

    Node* __find_node(int idx, long key) {
        Node* entry = m_table[idx];

        if (entry == NULL) {
            return NULL;
        }

        while (entry != NULL && entry->getKey() != key) {
            entry = entry->getNext();
        }

        return entry;
    }
    atomic_uint m_table_size{4096}; // it should be power of 2
    atomic_int _size{0};

    atomic_long _size_in_bytes{0};

    condition_variable_any con_var;
    std::hash<long> hasher;
    map_lock** locks;
    mutex thread_barier;
    bool rehash_enabled = false;
    atomic_int lock_id_sequencer = {1};

    void ___unlock(long key, int lock_id) {

        //        cout << "Index = " << key << endl;

        bool result = unlock(key, lock_id);
        if (!result) {
            //            cout << "Internal unlock fails. Lock_id=  " << lock_id << " Key = " << key;
        }
    }
};

#endif /* LOCK_FREE_MAP_H */


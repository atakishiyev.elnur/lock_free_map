/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   striped_map.h
 * Author: root
 *
 * Created on May 13, 2016, 2:04 AM
 */

#define _DEFAULT_CAPACITY  1000
#include <mutex>

#include <atomic>
#include <vector>
#include <jni.h>
#include <stdio.h>
#include <iostream>
#include <unordered_map>
#include <string.h>
#include <condition_variable>
#include <chrono>
#include <cmath>
#include <thread>
#include <obstack.h>
#include <stdlib.h>
#include <jemalloc/jemalloc.h>
#include <malloc.h>
#include "lock_free_map.h"
#include "value_c.h"
#include "linkedlogics_offheap_ConcurrentHashMap.h"

using namespace std;

lock_free_map** maps = new lock_free_map*[1024];
std::atomic_int idx{0};
std::mutex _mutex;

//

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_put__IJ_3BI
(JNIEnv * env, jobject, jint id, jlong key, jbyteArray value, jint version) {
    //unique_lock<mutex> m(_mutex);
    jboolean copy = true;
    jbyte* a = (jbyte*) env->GetPrimitiveArrayCritical(value, &copy);
    try {

        lock_free_map* map = maps[id];
        if (map) {
            int len = env->GetArrayLength(value);

            char * b = (char*) malloc(len);
            memmove(b, a, len);
            value_c *val_c = new value_c(key, len, b, version, map->eviction_data);

            bool result = map->put(key, val_c, 0);
            env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
            return result;

        } else {
            env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
            return false;
        }
    } catch (exception) {
        env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
        return false;
    }
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_put__IJ_3BJI
(JNIEnv * env, jobject, jint id, jlong key, jbyteArray value, jlong timeout, jint version) {
    //unique_lock<mutex> m(_mutex);
    jboolean copy = true;
    jbyte* a = (jbyte*) env->GetPrimitiveArrayCritical(value, &copy);
    try {
        lock_free_map* map = maps[id];
        if (map) {
            int len = env->GetArrayLength(value);

            char*b = (char*) malloc(len);
            memmove(b, a, len);

            value_c * val_c = new value_c(key, len, b, version, map->eviction_data);
            bool result = map->put(key, val_c, timeout);
            env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
            return result;
        } else {
            env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
            return false;
        }
    } catch (exception) {
        env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
        return false;
    }
}

JNIEXPORT jbyteArray JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_get
(JNIEnv * env, jobject, jint id, jlong key) {
    //unique_lock<mutex> m(_mutex);
    jbyteArray bArray = NULL;

    lock_free_map* map = maps[id];
    if (map) {
        value_c *v = map->get(key);

        if (v) {
            bArray = env->NewByteArray(v->getLength());
            if (bArray) {
                env->SetByteArrayRegion(bArray, 0, v->getLength(), (jbyte *) v->getValue());
            }
        }
    }
    return bArray;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_lock__IJJ
(JNIEnv *env, jobject, jint id, jlong key, jlong timeout) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->lock(key, timeout);
    }
    return false;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_lock__IJJI
(JNIEnv *, jobject, jint id, jlong key, jlong timeout, jint lock_id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->lock(key, timeout, lock_id);
    }

    return -1;
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_unlock
(JNIEnv *env, jobject, jint id, jlong key, jint lock_id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->unlock(key, lock_id);
    }
    return false;
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_remove__IJ
(JNIEnv *, jobject, jint id, jlong key) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->remove(key);
    }
    return false;
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_remove__IJI
(JNIEnv *, jobject, jint id, jlong key, jint version) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->remove(key, version);
    }
    return false;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_createMap__
(JNIEnv*, jobject) {

    std::unique_lock<mutex> lock;
    maps[idx] = new lock_free_map(4096);
    return idx++;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_createMap__I
(JNIEnv *, jobject, jint size) {

    std::unique_lock<mutex> lock;
    maps[idx] = new lock_free_map(size);
    return idx++;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_tryLock
(JNIEnv *, jobject, jint id, jlong key, jlong timeout) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->tryLock(key, timeout);
    }
    return false;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_size
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->size();
    }
    return -1;
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_sizeInBytes
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->size_in_bytes();
    }
    return -1;
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_resumeEviction
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        map->scheduler->resume();
    }
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_suspendEviction
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        map->scheduler->suspend();
        map->eviction_data->eviction_count = 0;
        map->eviction_data->total_scheduled.exchange(0);
    }
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_setEvictionHandler
(JNIEnv * env, jobject, jint id, jobject handler) {
    lock_free_map* map = maps[id];
    if (map) {
        //        map->eviction_data->listener = env->NewGlobalRef(handler);
        //        map->eviction_data->methodId = env->GetMethodID(env->GetObjectClass(handler), map_eviction_data::eviction_handler_method, map_eviction_data::eviction_handler_method_params);
        //        env->GetJavaVM(&map->eviction_data->jvm);
    }
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_rescheduleEviction
(JNIEnv *, jobject, jint id, jlong key, jlong timeout) {
    lock_free_map* map = maps[id];
    if (map) {
        value_c* value = map->get(key);
        if (value) {
            map->scheduler->cancel(value->task);
            value->task = map->scheduler->schedule(seconds(timeout), &lock_free_map::onExpired, map, key);
            return true;
        }
    }
    return false;
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_evictedCount
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->eviction_data->eviction_count;
    }
    return -1;
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_consumeEviction
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->consumeEvictionBlocking();
    }
    return -1;
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_consumeEvictionNonBlocking
(JNIEnv *, jobject, jint id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->consumeEvictionNonBlocking();
    }
    return -1;
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_checkLock
(JNIEnv *, jobject, jint id, jlong key, jint lock_id) {
    lock_free_map* map = maps[id];
    if (map) {
        return map->checkLock(key, lock_id);
    } else {
        return -1;
    }
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_printStat
(JNIEnv *, jobject, jint id) {
    //       lock_free_map* map = maps[id];
    //    if (map) {
    //        map->s

    //    }
}

/*
 * Class:     linkedlogics_offheap_ConcurrentHashMap
 * Method:    destroy
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_linkedlogics_offheap_ConcurrentHashMap_destroy
(JNIEnv *, jobject, jint id) {
    //    auto result = maps.find(id);
    //    if (result != maps.end()) {
    //        lock_free_map * map = result->second;
    //        maps.erase(result);
    //        delete map;
    //    }
}
